# your clothes 戲服
## 前奏

## 主歌-1

頭殼裝滿蟲

我最近一直在想

不管什麼時候

Headspace turns to worms

It's been on my mind lately

at any given moment



(尤其你經過時)

(When you grace past me)



是不是過太久？

你至少還記得名子

Or has it been too long?

At least you know my name



我總在情侶親吻的時候就換台

I always change the channel when the couple starts to kiss

## 導歌-A

難道都是一場秀？

我這演員看著真是個蠢

Was it all for a show?

Now I look so stupid for casting the roles



而當一切謝幕，我願我在你戲服裡頭

我願我在你戲服裡頭

And when the curtains close, I wish that I was in your clothes

I wish that I was in your clothes

## 副歌

這樣時，我試著不難過

一切都是在想你，而就給我這感謝？

Times like these, I try not to get upset

All I do is think about you, and this is the thanks I get?



時間如巴掌般的流逝過去

而現在你正跟個陌生人共走紅毯

Time passed with a slap to the back of my head

And now you're walking down the aisle with someone you've never met

## 背景: 副歌

## 導歌-B

難道都是一場秀？

我這演員看著真是個蠢

Was it all for a show?

Now I look so stupid for casting thе roles



而當故事結束，這到頭又是你的損失

這到頭又是你的損失

And when the story ends it's another loss for you again

it's another loss for you again

## 重複: 副歌

## 獨奏

## 橋段

我愛了個蠢蛋，不珍惜我愛

我看不清地板，這燈刺眼太

I'm in love with an airhead that popped my balloon

I can't see the floor, it's too bright in this room



我好難說話，又是我利可圖

你拈花惹草，復仇真是甜蜜

It gets so hard to speak, a commodity

You're picking your berries, revenge is so sweet

## 主歌-2

別裝著你不在乎，你我曾一起演出

Don't act like you didn't care 'cause I know you played a part



而我每逢你的車，我都感覺好反胃

And I start to feel so sick whenever I pass by your car



你說誰「你不喜歡」還「壞了你的心」

'Cause someone that "you don't like" just "broke your heart"



而他跟你說他不會太過頭

And he told you that he won't go very far



說你的未來不是只有藝術

Said your future don't revolve around art



我願我在你戲服裡頭

我願我在你戲服裡頭

我願我在你戲服裡頭

我願我在你戲服裡頭

I wish that I was in your clothes

I wish that I was in your clothes

I wish that I was in your clothes

I wish that I was in your clothes



我願我在你戲服裡頭

I wish that I was in your clothes

## 獨奏

## 尾奏