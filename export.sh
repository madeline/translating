#!/bin/sh

git add .
git commit -m "update"

python export.py

./fix_time.sh

cd pages
python3 ../scripts/index.py .
cd ..

python3 md_wrapper.py

git add pages
git commit -m "temp - this shouldn't be pushed"

git checkout pages

git rm -r .
git checkout main -- pages
shopt -s dotglob
mv ./pages/* .
rm -r ./pages
git add .
git commit --amend --no-edit
git push -f origin pages

git checkout main
git rm -r ./pages
git reset HEAD~1
